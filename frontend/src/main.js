import api from './api'
import Vue from 'vue'
import VueRouter from 'vue-router'
import vueResource from 'vue-resource'
import App from './App'
import Dashboard from './components/Dashboard'
const moment = require('moment');


Vue.use(require('vue-moment'), {
  moment
});
Vue.use(vueResource);
Vue.use(VueRouter);

Vue.prototype.$api = api;




const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    {path: '*', component: Dashboard},

  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
});



/* eslint-disable no-new */
new Vue({
  router,
  template: `
    <div id="app">        
    
      <div v-if="!authenticated">
        <router-view :key="$route.fullPath"></router-view>
      </div>
      
  </div>
      
    </div>
  `,
  data: {
    'authenticated': false,
  },
  methods: {

  },
  updated: function () {

  },
  mounted: function () {


  },
  watch: {

  }
}).$mount('#app');


