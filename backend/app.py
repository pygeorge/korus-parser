#! /var/www/emika-api/venv/bin/python

from flask import Flask
from backend.misc import crossdomain
from backend.components import account
from backend.components import admin
from backend.components import tasks
from backend.components import day_plan
from messenger.chat import chat


app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

app.register_blueprint(account)
app.register_blueprint(admin)
app.register_blueprint(tasks)
app.register_blueprint(day_plan)
app.register_blueprint(chat)


@app.route('/')
@crossdomain(origin='*')
def main_view():

    return """
        Korus Person Parser
        """


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=False)

